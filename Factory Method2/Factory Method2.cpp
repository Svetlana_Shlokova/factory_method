// Factory Method2.cpp: ���������� ����� ����� ��� ����������� ����������.
//

#include "stdafx.h"
#include <stdio.h>
#include<iostream>
#include<string>
#include <conio.h>
using namespace std;
 
// ����������� ����� "�������"
// ���������� ��������� ��������
class Product {
 public:
  virtual string getName() = 0;
  virtual double getPrice()= 0;
};
 
// ����� ���������� "�������" - "���������"
class Computer : public Product {
 public:
  string getName(){
    return "Computer";
  }
  double getPrice(){
    return 699.99;
  }
};
 
// ����� ���������� "�������" - "�������"
class Phone : public Product {
 public:
  string getName() {
    return "Phone";
  }
  double getPrice() {
    return 299.59;
  }
};
 
// "��������� ��������"
class Creator {
 public:
//��������� �����, ������������ ������ ���� "�������"
  virtual Product* CreateProduct() = 0;
};
 
// "��������� �����������"
class ComputerCreator : public Creator {
 public:
  Product* CreateProduct() {
    return new Computer();
  }  
};
 
// "��������� ���������"
class PhoneCreator : public Creator {
 public: 
  Product* CreateProduct() {
    return new Phone();
  }
};
 
int main() {
 const int size = 2;
// ������ ����������
 Creator* creators[size];
  creators[0] = new ComputerCreator();
  creators[1] = new PhoneCreator();    
// ����������� �������� ���������
 for(int i=0;i<size;i++) {
   Product* product = creators[i]->CreateProduct(); 
   cout<<"Product: "<<product->getName()<<endl;
   cout<<"Price: $"<<product->getPrice()<<endl;
   cout<<"\n";
   delete product;
 }
 for(int i=0;i<size;i++) {
   delete creators[i];
 }
 getch();
 return 0;    
}

